package dao;

import factory.Connect;
import org.testng.annotations.AfterClass;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAO {

    Connect conexao = new Connect();
    Statement stmt = conexao.abrirConexaoBancoDados();

    @AfterClass
    public void fechaBancoDados() throws SQLException {
        stmt.close();
    }

    public List<Map<String, Object>> consultaCompetencia() throws SQLException {
        String SQL = "SELECT Descricao, Instituicao FROM dbCOMPETENCIAS.dbo.CompetenciaPessoa";
        ResultSet rs = stmt.executeQuery(SQL);
        Map<String, Object> linha = null;
        List<Map<String, Object>> lista = new ArrayList<>();

        while (rs.next()) {
           linha = new HashMap<String, Object>();
          linha.put("Descricao", rs.getString("Descricao"));
          linha.put("Instituicao", rs.getString("Instituicao"));
          lista.add(linha);
       }
       return lista;
    }

}